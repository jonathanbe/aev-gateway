FROM kong

COPY conf/nginx.template .
COPY conf/kong.conf /etc/kong/kong.conf

CMD ["kong", "start", "--nginx-conf", "nginx.template"]
